
(require 'mmm-mode)

(mmm-add-classes
 '((asciidoctor-javascript
    :submode javascript-mode
    :face mmm-delcaration-submode-face
    :front "^begin_src$"
    :back "^end_src$")))

;;(setq mmm-global-mode 't)
;;(mmm-add-mode-ext-class 'fundamental-mode nil 'asciidoctor-javascript)










;;---------------------------------------------


;;; poly-adoc.el
;;
;; Filename: ply-adoc.el
;; Author: Daniel Shumway
;; Maintainer: Daniel Shumway
;; Keywords: emacs

;; (require 'polymode)

;; (defcustom pm-host/asciidoctor
;;   (pm-bchunkmode "asciidoctor"
;;                  :mode 'markdown-mode)
;;   "Asciidoctor host chunkmode"
;;   :group 'hostmodes
;;   :type 'object)

;; (defcustom pm-inner/javascript
;;   (pm-hbtchunkmode "javascript"
;;                    :mode 'js2-mode
;;                    :head-reg "begin_src"
;;                    :tail-reg "end_src"
;;                    :font-lock-narrow t)
;;   "Asciidoctor typical chunk."
;;   :group 'innermodes
;;   :type 'object)

;; (defcustom pm-poly/asciidoctor
;;   (pm-polymode-one "asciidoctor"
;;                    :hostmode 'pm-host/asciidoctor
;;                    :innermode 'pm-inner/javascript)
;;   "Asciidoctor typical polymode."
;;   :group 'polymodes
;;   :type 'object)

;; (define-polymode poly-asciidoctor-mode pm-poly/asciidoctor)
;; (add-to-list 'auto-mode-alist '("\\.adoc" . poly-asciidoctor-mode))




;;-------------------------------------------------

;;Base modes... I dunno how I feel about this crud.
;; (defcustom pm-base/doc
;;   (pm-submode "doc"
;;               :mode 'doc-mode)
;;   "AsciiDoctor base submode"
;;   :group 'base-submodes
;;   :type 'object)

;; (defcustom pm-config/doc
;;   (pm-config-one "doc"
;;                  :base-submode-name 'pm-base/doc
;;                  :inner-submode-name 'pm-submode/fundamental)
;;   "AsciiDoc typical configuration"
;;   :group 'polymode :type 'object)

;; ;;Asciidoctor setup
;; (defcustom pm-config/doc+js
;;   (clone pm-config/doc "doc+js" :inner-submode-name 'pm-submode/doc+js)
;;   "AsciiDoc + JS configuration"
;;   :group 'polymode :type 'object)

;; (defcustom pm-submode/doc+js
;;   (pm-inner-submode "doc+js"
;;                     :mode 'javascript-mode
;;                     :head-reg "//begin.jscode"
;;                     :tail-reg "//end.jscode")
;;   "AsciiDoc KnitJS submode."
;;   :group 'polymode :type 'object)

;; (define-polymode poly-doc+js-mode pm-config/doc+js)
;; (add-to-list 'auto-mode-alist '("\\.adoc" . poly-doc+js-mode))

;; (defcustom pm-host/asciidoctor
;;   (pm-bchunkmode "Asciidoctor"
;;                  :mode 'adoc-mode)
;;   "Asciidoctor host chunkmode"
;;   :group 'hostmodes
;;   :type 'object)

;; (defcustom pm-inner/asciidoctor
;;   (pm-inner-submode "asciidoctor"
;;                     :mode 'javascript-mode
;;                     :head-reg "//begin.js"
;;                     :tail-reg "//end.js"
;;                     :font-lock-narrow t)
;;   "Asciidoctor typical chunk."
;;   :group 'innermodes
;;   :type 'object)

;; (defcustom pm-poly/asciidoctor
;;   (pm-polymode-multi-auto "asciidoctor"
;;                           :hostmode 'pm-host/asciidoctor
;;                           :auto-innermode 'pm-inner/asciidoctor)
;;   "Asciidoctor typical configuration"
;;   :group 'polymodes
;;   :type 'object)

;; ;;;###autoload (autoload 'poly-adoc-mode "poly-adoc")
;; (define-polymode poly-adoc-mode pm-poly/asciidoctor)

;; (provide 'poly-adoc)
