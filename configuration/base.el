;;don't wrap lines in the editor (very annoying for most code)
(set-default 'truncate-lines t)

;;turn off spell-checking by default. Most of the time you don't want this
(setq-default dotspacemacs-configuration-layers
              '((spell-checking :variables spell-checking-enable-by-default nil)))

;;avoid keyboard-escape-quit blowing everything up in neotree
(defadvice keyboard-escape-quit
    (around keyboard-escape-quit-dont-close-windows activate)
  (let ((buffer-quit-function (lambda () ())))
    ad-do-it))

(setq version-control-diff-side 'left
      git-gutter-fr+-side 'left-fringe)
(global-git-gutter+-mode t)

;;don't chunk undo based on entering and exiting insert mode
(setq evil-want-fine-undo t)

;;fix window reset issue when exiting search.
(defun kill-minibuffer ()
  (interactive)
  (when (windowp (active-minibuffer-window))
    (evil-ex-search-exit)))

(add-hook 'mouse-leave-buffer-hook #'kill-minibuffer)

;;fix for helm/ido error in develop branch
(ido-mode -1)

;;Syntax highlighting at work.
(setq indent-tabs-mode nil)
(setq js2-basic-offset 2)

;;Make sure that saves happen in a separate directory.
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

(defun ask-user-about-lock (file other-user)
  "A value of t says to grab the lock on the file." t)
