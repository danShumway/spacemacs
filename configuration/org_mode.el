(setq-default org-log-done 'time);; log times when you finish a note
(setq-default org-log-done 'note);; log message when you mark a task as done
(setq-default org-enforce-todo-dependencies t);; Don't mark a task done if it has subtasks
(setq-default org-startup-indented t);; Pretty indentation
(setq-default org-support-shift-select 'always);; Normal keyboard controls for text selection.
(setq-default org-use-fast-todo-selection t);; Prompt TODO state (if states are available)

;;Get rid of some annoying keybindings
;; (eval-after-load 'org
;;   (progn
;;     (define-key org-mode-map (kbd "<S-left>") nil)
;;     (define-key org-mode-map (kbd "<S-right>") nil)))

;;better state highlighting
(setq-default org-todo-keyword-faces
      '(("REJECTED" . "blue") ("FAILED" . "yellow") ("INCORRECT" . "yellow")))

;;Auto-wrap text
(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))
(add-hook 'org-mode-hook (lambda () (setq word-wrap t)))

;; FONTS
;;--------
;; Set variable-pitch font using customize-face variable-pitch
;; Set the fonts to format correctly for specific modes. Default is set for fixed
;; so we only need to have the exceptions
(defun set-buffer-variable-pitch ()
  (interactive)
  (variable-pitch-mode t)
  (setq line-spacing 1)

  ;;Default built in
  (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
  ;;(set-face-attribute 'org-link nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-block nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-date nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-special-keyword nil :inherit 'fixed-pitch)

  ;;TODO keywords
  (set-face-attribute 'org-special-keyword nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-agenda-done nil :inherit 'fixed-pitch))

;; (add-hook 'org-mode-hook 'set-buffer-variable-pitch)

;; better text editing.
(add-hook 'org-mode-hook 'writeroom-mode) ;; center
(setq org-tags-column -60) ;; make sure tags still fit
(setq writeroom-global-effects '()) ;; disable everything else
(setq writeroom-mode-line t) ;; keep mode line
(setq writeroom-maximize-window nil) ;; don't maximize window
;; '(writeroom-fullscreen-effect (quote maximized))
;; '(writeroom-maximize-window nil)
;; '(writeroom-mode-line t)
;; '(writeroom-restore-window-config t)

;; helper functions for moving around
;; (defun org-archive-todo
;;     (let* ( ;; variables defined in `let` are not ordered. let* corrects this.
;;            (afile (org-extract-archive-file (org-get-local-archive-location))) ;; archive location
;;            (buffer (or (find-buffer-visiting afile) (find-file-noselect afile))) ;; actual buffer
;;            (tree-text))

;;       (org-mark-subtree)
;;       (setq tree-text (buffer-substring (region-beginning (region-end))))
;;       (org-end-of-subtree t t)))


;; (defun org-backlog-todo)
;; (defun org-schedule-todo)

