;;TODO: auto-detect this crap, allow faster toggling
(setq tab-width 2)
(setq indent-tabs-mode t)

;;web-mode >:(
(eval-after-load "web-mode"
  '(setq web-mode-markup-indent-offset 4)
  '(setq web-mode-css-indent-offset 4)
  '(setq web-mode-code-indent-offset 4)
  '(setq web-mode-indent-style 4)
	'(web-mode-use-tabs)
  )


;; (defun manual-tab () (interactive)
;;        (insert-tab)
;;        (print "inserting my tab"))

;; (defun manual-mode () (interactive)
;;        "Unbinds tab key and sets it to regular insertion"
;;        (global-set-key (kbd "<tab>") 'manual-tab))


;; Manual override
;; (global-set-key (kbd "<backtab>") (lambda () (interactive) (insert-tab)))
