#!/bin/bash

ln -sf $(pwd)/configuration/.spacemacs ~/.spacemacs
ln -sf $(pwd)/configuration/base.el ~/.emacs.d/private/base.el
ln -sf $(pwd)/configuration/keybindings.el ~/.emacs.d/private/keybindings.el
ln -sf $(pwd)/configuration/org_mode.el ~/.emacs.d/private/org_mode.el
ln -sf $(pwd)/configuration/dtrt-indent.el ~/.emacs.d/private/dtrt-indent.el
ln -sf $(pwd)/configuration/dtrt-indent-diag.el ~/.emacs.d/private/dtrt-indent-diag.el
ln -sf $(pwd)/configuration/transpose-frame.el ~/.emacs.d/private/transpose-frame.el
ln -sf $(pwd)/configuration/gimme-cat.el ~/.emacs.d/private/gimme-cat.el
ln -sf $(pwd)/configuration/indentation.el ~/.emacs.d/private/indentation.el
ln -sf $(pwd)/configuration/poly-adoc.el ~/.emacs.d/private/poly-adoc.el
ln -sf $(pwd)/configuration/minimap.el ~/.emacs.d/private/minimap.el
