# .spacemacs
My spacemacs setup, organized into branches:

## Installing

If you have node installed, you can simply run ``npm run install``. If you don't have node installed, you can manually run ``install/spacemacs.sh`` from the root directory of the project.

This will symlink the config onto the following locations:
 - ``~/.spacemacs``
 - ``~/.emacs/private/*.el``

It's a good idea to re-install every time you pull, but symlinks allow for fast, easy editing if you want add your own customizations

Branches are:
 - **linux** : My home and base setup. All other branches are based off of this.
 - **master** : stable version of ``linux``